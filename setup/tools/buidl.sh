#!/bin/bash

CUR_DIR=$(pwd -L)
MAKER="make"
TOOLCHAIN_DIR="/bin"
ROOTFS_DIR="/"
BUILD_RECIPE_FILE=""
PROJECT_DIR=""
PLATFORM_NAME_ENV=""
BIN_NAME=""
OUTPUT_DIR=""

source $(dirname $0)/utils.sh

x__print_usage() {
    cat <<EOF 1>&2
Usage:

    $0 [OPTIONS] <project_root>

Options
--------------
  --output      Place output in this directory
  --toolchain   Override the default toolchain
  --rootfs      Override the default rootfs
  --bin-name    name of the output binary
  --recipe      path to build recipe file
  --maker       what to use to build the project
                valid values:
                    - make (default)
                    - cmake
                    - cust (requires passing a build recipe file with --recipe)
  --help        Show this message

EOF
    exit 1
}

x__parse_args() {
    while [ $# -gt 0 ]; do
        local key="$1"
        case "$key" in
            --maker)
                MAKER=$2
                shift
                ;;
            --recipe)
                BUILD_RECIPE_FILE=$2
                shift
                ;;
            --output)
                OUTPUT_DIR=$2
                shift
                ;;
            --toolchain)
                TOOLCHAIN_DIR=$2
                shift
                ;;
            --rootfs)
                ROOTFS_DIR=$2
                shift
                ;;
            --bin-name)
                BIN_NAME=$2
                shift
                ;;
            --*)
                x__print_usage
                ;;
            *)
                PROJECT_DIR=$(x_realpath "$1")
                ;;
        esac
        shift
    done

    if [ -z $TOOLCHAIN_DIR ]; then
        FAIL_DIR_NAME="toolchain"
        FAIL_DIR_TYPE="not present"
    fi
    if [ ! -d $TOOLCHAIN_DIR ]; then
        FAIL_DIR_NAME="toolchain"
        FAIL_DIR_TYPE="not valid"
    fi
    if [ -z $ROOTFS_DIR ]; then
        FAIL_DIR_NAME="rootfs"
        FAIL_DIR_TYPE="not present"
    fi
    if [ ! -d $ROOTFS_DIR ]; then
        FAIL_DIR_NAME="rootfs"
        FAIL_DIR_TYPE="not valid"
    fi
    if [ -z $PROJECT_DIR ]; then
        FAIL_DIR_NAME="project"
        FAIL_DIR_TYPE="not present"
    fi
    if [ ! -d $PROJECT_DIR ]; then
        FAIL_DIR_NAME="project"
        FAIL_DIR_TYPE="not valid"
    fi

    if [ ! -z $FAIL_DIR_NAME ];then 
        echo "The $FAIL_DIR_NAME directory is $FAIL_DIR_TYPE!" 1>&2
        x__print_usage
    fi

    # grab the folder name and use it to guess the bin name
    BIN_NAME=${BIN_NAME:-$(echo $PROJECT_DIR | sed -e 's/^.*\///')}

}

x__cleanup() {
    export CFLAGS=""
    export LDFLAGS=""
    cd $CUR_DIR
}


x__parse_args $@

if [ ! -z $BUILD_RECIPE_FILE ]; then
    source $BUILD_RECIPE_FILE
fi

CROSS_COMPILE=$(find ${TOOLCHAIN_DIR} -name *gcc | awk -F/ '{print $NF}' | head -n 1 | sed 's/...$//')
CC="${TOOLCHAIN_DIR}/${CROSS_COMPILE}gcc"
CXX="${TOOLCHAIN_DIR}/${CROSS_COMPILE}g++"
CMAKE_FLAGS="-DCMAKE_SYSROOT=$ROOTFS_DIR \
             -DCMAKE_C_COMPILER=${CC} \
             -DCMAKE_CXX_COMPILER=${CXX}"

cd $PROJECT_DIR

case $MAKER in
    make)
        make -j

        if [ $? -eq 0 ]; then
            echo "BUILD SUCCESS"
            BUILT_DIR=$PROJECT_DIR
        else
            echo "make failed: couldn't build $BIN_NAME"
        fi
        ;;
    cmake)
        rm -rf build
        mkdir build
        cd build
        cmake $CMAKE_FLAGS ..
        if [ $? -eq 0 ]; then
            make -j

            if [ $? -eq 0 ]; then
                echo "BUILD SUCCESS"
                BUILT_DIR="${PROJECT_DIR}/build"
            else
                echo "make failed: couldn't build $BIN_NAME"
            fi
        else
            echo "cmake failed: couldn't build $BIN_NAME"
        fi
        ;;
    cust)
        cust_maker
        if [ $? -eq 0 ]; then
            echo "BUILD SUCCESS"
        else
            echo "custom maker failed: couldn't build $BIN_NAME"
        fi
        ;;
esac

if [ ! -z $OUTPUT_DIR ]; then
    if [ -e $BUILT_DIR/$BIN_NAME ]; then
        mkdir -p $OUTPUT_DIR
        cp $BUILT_DIR/$BIN_NAME $OUTPUT_DIR
    fi
fi


x__cleanup
