Copy-Item _vimrc_redirect $env:USERPROFILE
If(Test-Path (Join-Path $env:USERPROFILE '_vimrc')) {
    Remove-Item (Join-Path $env:USERPROFILE '_vimrc')
}
Rename-Item -Path (Join-Path $env:USERPROFILE '_vimrc_redirect') -NewName "_vimrc"
