" Maintainer:  Jacob Foell (jfoell@gmail.com)
" Last Change: 30-Sep-2015

" This colorscheme is based on a palette by username: "sugar!" on the site
" www.colourlovers.com.  It was the 3rd-most-favorited palette and seemed like
" it would work good with a dark background.  Besides the palette colors, there
" are also a few shades of grey employed.  The pallete was located here:
" http://www.colourlovers.com/palette/629637
"
set background=dark

hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "darksugar"

" General colors
hi CursorLine                   guibg=#2d2d2d
hi CursorColumn                 guibg=#2d2d2d
hi ColorColumn                  guibg=#2d2d2d
hi MatchParen     guifg=#c8c8a9 guibg=#fe4365 gui=bold
hi Pmenu          guifg=#f6f3e8 guibg=#444444
hi PmenuSel       guifg=#000000 guibg=#cae682
hi Cursor         guifg=NONE    guibg=#656565 gui=none
hi CursorLineNr   guifg=#fc9d9a guibg=NONE    gui=bold
hi Normal         guifg=#c8c8a9 guibg=#242424 gui=none
hi NonText        guifg=#000000 guibg=NONE    gui=none
hi LineNr         guifg=#857b6f guibg=#000000 gui=none
hi StatusLine     guifg=#f9cdad guibg=#444444 gui=bold
hi StatusLineNC   guifg=#857b6f guibg=#444444 gui=none
hi VertSplit      guifg=#444444 guibg=#444444 gui=none
hi Folded         guifg=#fe4365 guibg=#000000 gui=none
hi FoldColumn     guifg=#fe4365 guibg=#000000 gui=none
hi Title          guifg=#f6f3e8 guibg=NONE    gui=bold
hi Visual         guifg=#f6f3e8 guibg=#444444 gui=none
hi SpecialKey     guifg=#000000 guibg=NONE    gui=none
hi Directory      guifg=#83af9b guibg=NONE    gui=bold
hi Question       guifg=#fc9d9a guibg=NONE    gui=bold
hi Search         guifg=#c8c8a9 guibg=#fe4365 gui=bold
hi ErrorMsg       guifg=#fe4365 guibg=NONE    gui=underline
hi WarningMsg     guifg=#fc9d9a guibg=NONE    gui=underline
hi MoreMsg        guifg=#83af9b guibg=NONE    gui=bold
hi ModeMsg        guifg=#c8c8a9 guibg=NONE    gui=bold

" Syntax highlighting
hi Comment        guifg=#99968b               gui=none
hi Todo           guifg=#fe4365 guibg=NONE    gui=bold
hi Constant       guifg=#f9cdad               gui=none
hi String         guifg=#fc9d9a               gui=none
hi Identifier     guifg=#fc9d9a               gui=none
hi Function       guifg=#f9cdad               gui=bold
hi Type           guifg=#c8c8a9               gui=bold
hi Statement      guifg=#83af9b               gui=none
hi Keyword        guifg=#83af9b               gui=bold
hi PreProc        guifg=#fe4365               gui=none
hi Number         guifg=#f9cdad               gui=none
hi Special        guifg=#fc9d9a               gui=none
hi Underlined     guifg=#83af9b guibg=NONE    gui=underline
hi Error          guifg=#fc9d9a guibg=NONE    gui=bold

" Diff colors
hi DiffAdd        guifg=NONE    guibg=#444444 gui=none
hi DiffChange     guifg=NONE    guibg=#444444 gui=none
hi DiffDelete     guifg=#83af9b guibg=#83af9b gui=none
hi DiffText       guifg=NONE    guibg=#000000

