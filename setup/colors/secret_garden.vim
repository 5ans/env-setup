" Maintainer:  Jacob Foell (jfoell@gmail.com)
" Last Change: 08-Mar-2018

set background=dark

hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "secret_garden"

"" General colors
hi CursorLine                   guibg=#2d2d2d
hi CursorColumn                 guibg=#2d2d2d
hi ColorColumn                  guibg=#2d2d2d
hi MatchParen     guifg=#f6f1de guibg=#95a131 gui=bold
hi Pmenu          guifg=#f6f3e8 guibg=#444444
hi PmenuSel       guifg=#000000 guibg=#cae682
hi Cursor         guifg=NONE    guibg=#656565 gui=none
hi CursorLineNr   guifg=#c8cd3b guibg=NONE    gui=bold
hi Normal         guifg=#f6f1de guibg=#242424 gui=none
hi NonText        guifg=#000000 guibg=NONE    gui=none
hi LineNr         guifg=#857b6f guibg=#000000 gui=none
hi StatusLine     guifg=#f5b9ae guibg=#444444 gui=bold
hi StatusLineNC   guifg=#857b6f guibg=#444444 gui=none
hi VertSplit      guifg=#444444 guibg=#444444 gui=none
hi Folded         guifg=#95a131 guibg=#000000 gui=none
hi FoldColumn     guifg=#95a131 guibg=#000000 gui=none
hi Title          guifg=#f6f3e8 guibg=NONE    gui=bold
hi Visual         guifg=#f6f3e8 guibg=#444444 gui=none
hi SpecialKey     guifg=#000000 guibg=NONE    gui=none
hi Directory      guifg=#ee0b5b guibg=NONE    gui=bold
hi Question       guifg=#c8cd3b guibg=NONE    gui=bold
hi Search         guifg=#f6f1de guibg=#95a131 gui=bold
hi ErrorMsg       guifg=#95a131 guibg=NONE    gui=underline
hi WarningMsg     guifg=#c8cd3b guibg=NONE    gui=underline
hi MoreMsg        guifg=#ee0b5b guibg=NONE    gui=bold
hi ModeMsg        guifg=#f6f1de guibg=NONE    gui=bold

"" Syntax highlighting
hi Comment        guifg=#99968b             gui=italic
hi Todo           guifg=#95a131 guibg=NONE  gui=bold
hi Constant       guifg=#f5b9ae             gui=none
hi String         guifg=#c8cd3b             gui=italic
hi Identifier     guifg=#c8cd3b             gui=none
hi Function       guifg=#f5b9ae             gui=bold
hi Type           guifg=#f6f1de             gui=bold
hi Statement      guifg=#ee0b5b             gui=none
hi Keyword        guifg=#ee0b5b             gui=bold
hi PreProc        guifg=#95a131             gui=none
hi Number         guifg=#f5b9ae             gui=none
hi Special        guifg=#c8cd3b             gui=none
hi Underlined     guifg=#ee0b5b guibg=NONE  gui=underline
hi Error          guifg=#c8cd3b guibg=NONE  gui=bold

"" Diff colors
hi DiffAdd        guifg=NONE    guibg=#444444 gui=none
hi DiffChange     guifg=NONE    guibg=#444444 gui=none
hi DiffDelete     guifg=#ee0b5b guibg=#ee0b5b gui=none
hi DiffText       guifg=NONE    guibg=#000000

