#include "quectel_serial.h"
#include "quectel_debug.h"

#define INVALID_PORT_HANDLE_VALUE 0
#define LOG_BUFFER_SIZE 1024

static com_port_t com_port;
extern int debug;

/*
 * Copy src to string dst of size siz.  At most siz-1 characters
 * will be copied.  Always NUL terminates (unless siz == 0).
 * Returns strlen(src); if retval >= siz, truncation occurred.
 */
size_t strlcpy(char *dst, const char *src, size_t siz)
{
    char *d = dst;
    const char *s = src;
    size_t n = siz;

    /* Copy as many bytes as will fit */
    if (n != 0)
    {
        while (--n != 0)
        {
            if ((*d++ = *s++) == '\0')
                break;
        }
    }

    /* Not enough room in dst, add NUL and traverse rest of src */
    if (n == 0)
    {
        if (siz != 0)
            *d = '\0'; /* NUL-terminate dst */
        while (*s++)
            ;
    }

    return (s - src - 1); /* count does not include NUL */
}

/*
 * Appends src to string dst of size siz (unlike strncat, siz is the
 * full size of dst, not space left).  At most siz-1 characters
 * will be copied.  Always NUL terminates (unless siz <= strlen(dst)).
 * Returns strlen(src) + MIN(siz, strlen(initial dst)).
 * If retval >= siz, truncation occurred.
 */
size_t strlcat(char *dst, const char *src, size_t siz)
{
    char *d = dst;
    const char *s = src;
    size_t n = siz;
    size_t dlen;

    /* Find the end of dst and adjust bytes left but don't go past end */
    while (n-- != 0 && *d != '\0')
        d++;
    dlen = d - dst;
    n = siz - dlen;

    if (n == 0)
        return (dlen + strlen(s));
    while (*s != '\0')
    {
        if (n != 1)
        {
            *d++ = *s;
            n--;
        }
        s++;
    }
    *d = '\0';

    return (dlen + (s - src)); /* count does not include NUL */
}

static void print_buffer(byte *buffer, size_t length, int bytes_to_print)
{
    size_t i = 0;
    char temp_output_buffer[LOG_BUFFER_SIZE] = {0};
    char output_buffer[LOG_BUFFER_SIZE] = {0};

    for (i = 0; i < MIN(length, (size_t)bytes_to_print); ++i)
    {
        snprintf(temp_output_buffer, sizeof(temp_output_buffer), "%02X ", buffer[i]);
        strlcat(output_buffer, temp_output_buffer, sizeof(output_buffer));
    }
    printf("%s\n", output_buffer);
#if 0
    if (length > (size_t) bytes_to_print) {
        output_buffer[0] = '\0';
        for (i = 0; i < (size_t) bytes_to_print; ++i) {
            snprintf(temp_output_buffer, sizeof(temp_output_buffer), "%02X ", buffer[(length - bytes_to_print) + i]);
            strlcat(output_buffer, temp_output_buffer, sizeof(output_buffer));
        }
        dbg("Last few bytes: %s", output_buffer);
    }
#endif
}

void port_disconnect()
{
    if (INVALID_PORT_HANDLE_VALUE == com_port.port_fd)
        return;

    dbg("Disconnecting from com port");
    if (com_port.port_fd != INVALID_PORT_HANDLE_VALUE)
    {
        close(com_port.port_fd);
        com_port.port_fd = INVALID_PORT_HANDLE_VALUE;
    }
}

int port_connect(char *port_name)
{
    struct termios tio;
    struct termios settings;
    int using_tty_device;
    int retval;

    if (NULL == port_name || port_name[0] == '\0')
    {
        dbg("invalid port name: %s", port_name);
        return -1;
    }
    com_port.port_name = port_name;
    
    /* Close any existing open port */
    if (com_port.port_fd != INVALID_PORT_HANDLE_VALUE)
        port_disconnect();

    using_tty_device = (strspn(port_name, "/dev/tty") == strlen("/dev/tty"));

    // Opening the com port
    // Change for Android port - Opening in O_SYNC mode since tcdrain() is not supported
    //com_port.port_fd = open (port_name, O_RDWR | O_SYNC | O_NONBLOCK);
    com_port.port_fd = open(port_name, O_RDWR | O_SYNC);

    if (INVALID_PORT_HANDLE_VALUE == com_port.port_fd)
        return -1;

    if (1 == using_tty_device)
    {
        dbg("USING UART DETECTED pPort='%s'", port_name);
        dbg("Configuring 115200, 8n1");

        memset(&tio, 0, sizeof(tio));
        tio.c_iflag = 0;
        tio.c_oflag = 0;
        tio.c_cflag = CS8 | CREAD | CLOCAL; // 8n1, see termios.h for more information
        tio.c_lflag = 0;
        tio.c_cc[VMIN] = 1;
        tio.c_cc[VTIME] = 5;
        cfsetospeed(&tio, B115200); // 115200 baud
        cfsetispeed(&tio, B115200); // 115200 baud
        tcsetattr(com_port.port_fd, TCSANOW, &tio);
    }

    if (using_tty_device)
    {
        // Configure termios settings

        // Get the COM port settings
        retval = tcgetattr(com_port.port_fd, &settings);
        if (-1 == retval)
        {
            dbg("termio settings could not be fetched Linux System Error:%s", strerror(errno));
            return -1;
        }

        // CREAD to enable receiver and CLOCAL to say that the device is directly connected to the host
        cfmakeraw(&settings);
        settings.c_cflag |= CREAD | CLOCAL;
#if 0
        // configure the new settings
        if (com_port.protocol != SAHARA_PROTOCOL)
            tcflush (com_port.port_fd, TCIOFLUSH);
#endif
        retval = tcsetattr(com_port.port_fd, TCSANOW, &settings);
        if (-1 == retval)
        {
            dbg("Device could not be configured: Linux System Errno: %s",
                strerror(errno));
            return -1;
        }
    }

    return 0;
}

int tx_data(byte *buffer, size_t bytes_to_send)
{
    int temp_bytes_sent;
    size_t bytes_sent = 0;
    if (debug)
        dbg("tx %ld bytes>", bytes_to_send);
    if (debug && bytes_to_send < 128)
        print_buffer(buffer, bytes_to_send, 128);

    while (bytes_sent < bytes_to_send)
    {

        temp_bytes_sent = write(com_port.port_fd, buffer + bytes_sent, MIN(bytes_to_send - bytes_sent, /*com_port.MAX_TO_WRITE*/ 1024 * 10));
        if (temp_bytes_sent < 0)
        {
            dbg("Write returned failure %d, errno %d, System error code: %s", temp_bytes_sent, errno, strerror(errno));
            return -1;
        }
        else
        {
            bytes_sent += temp_bytes_sent;
        }
    }
    return 0;
}
int rx_data(byte *buffer, size_t bytes_to_read, size_t *bytes_read)
{
    fd_set rfds;
    struct timeval tv;
    int retval;

    // Init read file descriptor
    FD_ZERO(&rfds);
    FD_SET(com_port.port_fd, &rfds);

    // time out initializtion.
    tv.tv_sec = 3;
    tv.tv_usec = 0;

    retval = select(com_port.port_fd + 1, &rfds, NULL, NULL, &tv);
    if (0 == retval)
    {
        dbg("Timeout Occured, No response or command came from the target!");
        return -1;
    }
    if (retval < 0)
    {
        dbg("select returned error: %s", strerror(errno));
        return -1;
    }

    retval = read(com_port.port_fd, buffer, bytes_to_read);
    if (0 == retval)
    {
        dbg("Zero length packet received or hardware connection went off");
        return -1;
    }
    else if (retval < 0)
    {
        // tcflush (com_port.port_fd, TCIOFLUSH);
        if (EAGAIN == errno)
        {
            // Sleep before retrying in case we were using
            // non-blocking reads
            usleep(1000 * 500);
        }
        else
        {
            dbg("Read/Write File descriptor returned error: %s, error code %d", strerror(errno), retval);
            return -1;
        }
    }

    if (NULL != bytes_read)
        *bytes_read = retval;

    if (debug)
        dbg("Rx %d bytes<", retval);
    if (debug && retval < 128)
    {
        print_buffer(buffer, (size_t)retval, 128);
    }
    return 0;
}
