#ifndef QUECTEL_DEBUG_H
#define QUECTEL_DEBUG_H

#include "quectel_include.h"

void dbg_time(const char *fmt, ...);
#ifdef NDEBUG
#define dbg dbg_time
#else
#define dbg(fmt, args...) dbg_time("[%16s:%-4d] " fmt, __FILE__, __LINE__, ##args)
#endif

#endif
